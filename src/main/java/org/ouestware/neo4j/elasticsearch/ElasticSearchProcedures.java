package org.ouestware.neo4j.elasticsearch;

import io.searchbox.action.BulkableAction;
import io.searchbox.core.Bulk;
import io.searchbox.core.Delete;
import org.neo4j.common.DependencyResolver;
import org.neo4j.configuration.Config;
import org.neo4j.graphdb.*;
import org.neo4j.logging.Log;
import org.neo4j.procedure.*;
import org.ouestware.neo4j.elasticsearch.config.ElasticSearchConfig;
import org.ouestware.neo4j.elasticsearch.model.DocumentIndexId;
import org.ouestware.neo4j.elasticsearch.model.IndexAllResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class ElasticSearchProcedures {

    private final static Long DEFAULT_BATCH_SIZE = Long.valueOf(500);
    private final static Boolean DEFAULT_ASYNC = Boolean.FALSE;

    @Context
    public GraphDatabaseService db;

    @Context
    public DependencyResolver dependencyResolver;

    @Context
    public Transaction tx;

    @Context
    public Log log;

    private static Long getBatchSize(Map<String, Object> config) {
        return (Long) config.getOrDefault("batchSize", DEFAULT_BATCH_SIZE);
    }

    private static Boolean getAsync(Map<String, Object> config) {
        return (Boolean) config.getOrDefault("async", DEFAULT_ASYNC);
    }

    private static List<String> getIndices(Map<String, Object> config) {
        return (List<String>) config.getOrDefault("indices", null);
    }

    @Procedure(value = "elasticsearch.index", mode = Mode.SCHEMA)
    @Description("elasticsearch.index(labels, { batchSize:500, async:false }) - Index all the node of the specified labels")
    public Stream<IndexAllResult> index(@Name("label") List<String> labels, @Name("config") Map<String, Object> config) throws Exception {
        return Stream.of(index(labels, getAsync(config), getBatchSize(config)));
    }

    @Procedure(value = "elasticsearch.indexAll", mode = Mode.SCHEMA)
    @Description("elasticsearch.indexAll({ batchSize:500, async:false }) - Index all the node of the specified labels")
    public Stream<IndexAllResult> indexAll(@Name("config") Map<String, Object> config) throws Exception {
        List<String> labels = new ArrayList<>();
        for (Label label : tx.getAllLabels()) {
            labels.add(label.name());
        }
        return index(labels, config);
    }

    @Procedure(value = "elasticsearch.indexNodes", mode = Mode.SCHEMA)
    @Description("elasticsearch.indexNodes(nodes, { async:false }) - Index all the specified nodes in one batch")
    public Stream<IndexAllResult> indexNodes(@Name("nodes") List<Node> nodes, @Name("config") Map<String, Object> config) throws Exception {
        // init variables
        Map<DocumentIndexId, BulkableAction> actions = new HashMap<>(nodes.size());
        long nbDoc = 0;

        if (nodes.size() > 0) {
            // Create the ES client
            ElasticSearchConfig esConfig = this.getEsConfig();
            try (ElasticSearchClient client = new ElasticSearchClient(esConfig)) {
                for (Node node : nodes) {
                    Map<DocumentIndexId, BulkableAction> indexRequests = client.indexRequestsAction(node, tx);
                    if (indexRequests.size() > 0) {
                        actions.putAll(indexRequests);
                        nbDoc++;
                    }
                }
                if (actions.size() > 0) {
                    Bulk bulk = new Bulk.Builder().addAction(actions.values()).build();
                    client.index(bulk, getAsync(config));
                    actions.clear();
                }
            } catch (Exception e) {
                throw new RuntimeException("Failed to index nodes", e);
            }
        }

        return Stream.of(new IndexAllResult(1, nbDoc));
    }

    @Procedure(value = "elasticsearch.deleteNodes", mode = Mode.SCHEMA)
    @Description("elasticsearch.deleteNodes(nodes, { async:false }) - Delete all the specified nodes in one batch")
    public Stream<IndexAllResult> deleteNodes(@Name("nodes") List<Node> nodes, @Name("config") Map<String, Object> config) throws Exception {
        // init variables
        Map<DocumentIndexId, Delete> actions = new HashMap<>(nodes.size());
        long nbDoc = 0;

        if (nodes.size() > 0) {
            // Create the ES client
            ElasticSearchConfig esConfig = this.getEsConfig();
            try (ElasticSearchClient client = new ElasticSearchClient(esConfig);) {
                for (Node node : nodes) {
                    Map<DocumentIndexId, Delete> deleteRequests = client.deleteRequestsAction(node);
                    if (deleteRequests.size() > 0) {
                        actions.putAll(deleteRequests);
                        nbDoc++;
                    }
                }
                if (actions.size() > 0) {
                    Bulk bulk = new Bulk.Builder().addAction(actions.values()).build();
                    client.index(bulk, getAsync(config));
                    actions.clear();
                }
            } catch (Exception e) {
                throw new RuntimeException("Failed to delete nodes", e);
            }
        }

        return Stream.of(new IndexAllResult(1, nbDoc));
    }

    @Procedure(value = "elasticsearch.deleteNodesByElementId", mode = Mode.SCHEMA)
    @Description("elasticsearch.deleteNodesByElementId(nodes, { async:false, indices: ['my-index']}) - Delete all the specified nodes in one batch")
    public Stream<IndexAllResult> deleteNodesByElementId(@Name("nodes") List<String> nodes, @Name("config") Map<String, Object> config) throws Exception {
        // init variables
        Map<DocumentIndexId, Delete> actions = new HashMap<>(nodes.size());
        long nbDoc = 0;

        if (nodes.size() > 0) {
            // Create the ES client
            ElasticSearchConfig esConfig = this.getEsConfig();
            try (ElasticSearchClient client = new ElasticSearchClient(esConfig);) {
                for (String node : nodes) {
                    Map<DocumentIndexId, Delete> deleteRequests = client.deleteRequestsAction(getIndices(config), node);
                    if (deleteRequests.size() > 0) {
                        actions.putAll(deleteRequests);
                        nbDoc++;
                    }
                }
                if (actions.size() > 0) {
                    Bulk bulk = new Bulk.Builder().addAction(actions.values()).build();
                    client.index(bulk, getAsync(config));
                    actions.clear();
                }
            } catch (Exception e) {
                throw new RuntimeException("Failed to delete nodes by id", e);
            }
        }

        return Stream.of(new IndexAllResult(1, nbDoc));
    }

    private IndexAllResult index(List<String> labels, boolean async, long batchSize) throws Exception {

        // init variables
        Map<DocumentIndexId, BulkableAction> actions = new HashMap<>(1000);
        long nbBatch = 0;
        long nbDoc = 0;

        // Create the ES client
        ElasticSearchConfig esConfig = this.getEsConfig();
        try (ElasticSearchClient client = new ElasticSearchClient(esConfig);) {
            // For all labels check if it's an indexed label
            for (String label : labels) {
                if (client.config.indices.containsKey(label)) {
                    log.info("Starting bulk es indexation for label %s", label);
                    // retrieve all the node with the label
                    try (ResourceIterator<Node> nodes = tx.findNodes(Label.label(label))) {
                        while (nodes.hasNext()) {
                            Node node = nodes.next();
                            nbDoc++;
                            actions.putAll(client.indexRequestsAction(node, tx));

                            if (actions.size() == batchSize || (!nodes.hasNext() && actions.size() > 0)) {
                                Bulk bulk = new Bulk.Builder().addAction(actions.values()).build();
                                client.index(bulk, async);
                                actions.clear();
                                nbBatch++;
                                log.debug("Bulk es indexation batch number %d (nb docs %d)", nbBatch, nbDoc);
                            }
                        }
                    }
                }
            }

            return new IndexAllResult(nbBatch, nbDoc);
        } catch (Exception e) {
            throw new RuntimeException("Failed to re index", e);
        }
    }

    private ElasticSearchConfig getEsConfig() throws Exception {
        Config neoConfig = dependencyResolver.resolveDependency(Config.class);
        ElasticSearchConfig esConfig = new ElasticSearchConfig(neoConfig, db.databaseName());
        if(!esConfig.isEnabled) throw new Exception(String.format("ElasticSearch configuration is not defined for database %s",db.databaseName()));
        return esConfig;
    }

}
